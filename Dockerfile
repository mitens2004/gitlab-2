FROM node:carbon-alpine
WORKDIR /appdocker
ARG GIT_COMMIT
ENV GIT_COMMIT = $GIT_COMMIT
ARG TIME
ENV TIME = $TIME
COPY app.js /appdocker
COPY package.json /appdocker
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]
