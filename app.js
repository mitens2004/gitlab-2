const express = require('express')
const app = express()
const port = 3000

const hash = process.env.GIT_COMMIT
const fecha = process.env.TIME

app.get('/', (req, res) => {
  res.send("Hola mundo" + "\n" + "v. " + fecha + "/" + hash)
})

app.listen(port)

module.exports=app


